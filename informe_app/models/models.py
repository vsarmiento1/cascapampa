# -*- coding: utf-8 -*-

from odoo import models, fields, api
from datetime import datetime, timedelta
 
class informeapp(models.Model):
    _name = "informe.app" # nombre de la base de datos: todo_app
    _description = "Lista de informe tecnico"
    #_inherit="res.partner"
    name=fields.Char(string="Cliente1")
    #se enlaza con la tabla clientes
    partner_id = fields.Many2one ('res.partner',string='Cliente',required=True)
    sequence_number = fields.Integer(string='Sequence Number')
    sequence_number_padded = fields.Char(string='Padded Sequence Number', compute='_compute_sequence_number_padded')
    dateing = fields.Date(string="Fecha Ingreso",required=True)
    dateemi = fields.Date(string='Fecha Emisión', default=fields.Date.today)
    fechaentrega = fields.Date(string="Fecha de entrega")
    datega = fields.Date(string="Fecha Garantia")
    atencion = fields.Char(string="Atención")
    orden = fields.Char(string="Orden de trabajo")
    placa = fields.Char(string="Placa")
    #mcom = fields.Char("Codigo")
    #mbag = fields.Char("BAG")
    #mcdi = fields.Char("CDI")
    tipo_s = fields.Selection(selection=[("COM","COM"),("BAG","BAG"),("CDI","CDI")],string="Tipo de Serie",default="COM",required=True)
    #bus = fields.Char(string="Bus")
    #se enlaza con la tabla de buses
    bus_id = fields.Many2one ('informe.bus',string='Vehiculo')
    garantia_id = fields.Many2one ('informe.categoria',string='Dias de garantia')
    prod_id=fields.Many2one ('informe.prod',string='Componente')
    estado=fields.Char(string="Estado", compute="_compute_estado")
    #componente = fields.Char(string="Componente")
    serie = fields.Char(string="Serie",required=True)
    kilometraje = fields.Char(string="Kilometraje")
    informe_cliente = fields.Text(string="Informe cliente")
    evaluacion = fields.Text(string="Evaluación")
    nota = fields.Text(string="Nota")
    lista_image1_ids= fields.One2many("informe.ima","image_id")
    lista_image2_ids= fields.One2many("informe.imapie","image_id")
    lista_image3_ids= fields.One2many("informe.imadocadic","image_id")
    lista_image4_ids= fields.One2many("informe.imadogarantia","image_id")
    def _default_partner(self):
         return self.env['res.partner'].search([('name', '=', 'NAME OF THE PARTNER')], limit=1).id
    @api.model
    @api.depends('sequence_number')
    def _compute_sequence_number_padded(self):
        for record in self:
            record.sequence_number_padded = str(record.id).zfill(7)
    @api.depends('serie')
    def _compute_estado(self):
        current_date = fields.Date.today()
        for record in self:
            if  current_date > record.datega:
                record.estado = "Sin garantia"
            else:
                record.estado = "En garantia"
    # Onchange para actualizar la placa del bus
    @api.onchange('bus_id')
    def onchange_bus_id(self):
            if self.bus_id:
                if self.bus_id.placa:
                    self.placa=self.bus_id.placa
            else:
                self.placa = ""     
    @api.onchange('tipo_s')
    def onchange_prod_id(self):
            if self.tipo_s:
                if self.tipo_s:
                    self.serie=self.tipo_s+"-"
            else:
                self.tipo_s = ""                                
    @api.onchange('garantia_id')
    def onchange_garantia_id(self):
            if self.garantia_id:
                if self.garantia_id.name:
                    fecha_inicio = fields.Date.from_string(self.fechaentrega)
                    total_dias=self.garantia_id.name
                    nuevo = timedelta(days = total_dias)
                    fecha_garantia = fecha_inicio+nuevo
                    self.datega = fecha_garantia
            else:
                self.name = ""  
               
class informreimage(models.Model):
    _name= "informe.ima"
    _description = "Imagenes de informe"
    #name= fields.Char("Nombre")
    description= fields.Char("Observación")
    image= fields.Binary(attachment=True)
    image_id=fields.Many2one("informe.app")

class informreimage(models.Model):
    _name= "informe.imapie"
    _description = "Imagenes de informe"
    #name= fields.Char("Nombre")
    description= fields.Char("Observación")
    image= fields.Binary(attachment=True)
    image_id=fields.Many2one("informe.app")  

class informreimage(models.Model):
    _name= "informe.imadocadic"
    _description = "Imagenes adicionales"
    #name= fields.Char("Nombre")
    description= fields.Char("Observación")
    image= fields.Binary(attachment=True)
    image_id=fields.Many2one("informe.app")        
class informreimagegarantia(models.Model):
    _name= "informe.imadogarantia"
    _description = "Imagenes adicionales"
    #name= fields.Char("Nombre")
    dateemi = fields.Date(string="Fecha de regirtro",required=True)
    description= fields.Char("Observación")
    image= fields.Binary(attachment=True)
    image_id=fields.Many2one("informe.app")  

class informebus(models.Model):
    _name= "informe.bus"
    _description = "Lista de buses"
    name = fields.Char("Numero",required=True)
    #name  = fields.Char("Nombre",required=True)
    placa = fields.Char("Placa")
   #myear = fields.Char("Año",required=True)
    tipo = fields.Selection(selection=[("BUS","BUS"),("PADRON","PADRON")],string="Tipo",default="BUS",required=True)
    #mserie = fields.Char("SERIE",required=True)
    #mcom = fields.Char("COM",required=True)
    #mbag = fields.Char("BAG",required=True)
    #mcdi = fields.Char("CDI",required=True)
    ###############################################
    #categoria_id = fields.Many2one ('informe.categoria',string='Categoria')
    #modelo_id = fields.Many2one ('informe.modelo',string='Modelo')
    #marca_id = fields.Many2one ('informe.marca',string='Marca')
    ###############################################
    bus_id=fields.Many2one("informe.app")
    @api.onchange('tipo')
    def onchange_prod_id(self):
            if self.tipo:
                if self.tipo:
                    self.name=self.tipo+"-"
            else:
                self.name = ""  

class informecate(models.Model):
    _name= "informe.categoria"
    _description = "Lista de garantias"
    name= fields.Integer("Dias",required=True) 
    garantia_id=fields.Many2one("informe.app")

class informeprod(models.Model):
    _name= "informe.prod"
    _description = "Lista de Componentes"
    name= fields.Char("Nombre",required=True)   
    componente_id=fields.Many2one("informe.app")

class informemarca(models.Model):
    _name= "informe.marca"
    _description = "Lista de marca"
    name= fields.Char("Nombre",required=True)
    marca_id=fields.Many2one("informe.bus")

class SaleOrder(models.Model):
    _inherit = "sale.order"
    #canal_ventas = fields.Char("Canal de ventas")
    bus_id = fields.Many2one ('informe.bus',string='Bus')
    _placa = fields.Char('Placa')
    dcotizacion = fields.Text('Detalle de Cotizacion')	
    dobra = fields.Text('Mano de Obra 1')	
    @api.onchange('bus_id')
    def onchange_bus_id(self):
            if self.bus_id:
                if self.bus_id.placa:
                    self._placa=self.bus_id.placa
            else:
                self._placa = ""      