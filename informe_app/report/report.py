from odoo import models,api
from datetime import datetime

class ReportDetalleMovimiento(models.AbstractModel):
    _name = "report.informe_app.report_informe"

    @api.model
    def _get_report_values(self,docids,data=None):
        docs = self.env["informe.app"].browse(docids)
        docargs= {
            "docs":docs,
            "fecha": datetime.now().strftime("%m-%d-%Y")
        }

        return docargs